# Installation Instructions
To perform the programming exercises, you need:
* GIT Client
* BASH command-line (e.g. GIT Bash)
* Java JDK 
* IntelliJ IDEA or MS Visual Code
* SDKMAN (optional)
* Gradle (optional)


# Install GIT
Unless you alread have it, start by installing GIT. As part of the
install it asks if it should install BASH; say *yes*.

* [Git Client](https://git-scm.com/downloads)

# Install an IDE
You need an IDE to edit your source code for the exercises.
Choose any of:

* [Microsoft Visual Code](https://code.visualstudio.com/download)
* [JetBrains IntellJ IDEA](https://www.jetbrains.com/idea/download)


# Install SDKMAN (*optional*)
This is an installer for JVM based tools we will use it to install all Java related tools and frameworks. It only works
within a BASH shell, such as GIT-BASH for Windows, WSL @ Windows-10 or a Linux machine.

## Prepare
Before you install SDKMAN within GIT-BASH, you need to augment MinGW BASH with a ZIP tool. Locate and download the 

* [ZIP bundle from GnuWin32](https://sourceforge.net/projects/gnuwin32/files/zip/3.0/zip-3.0-bin.zip/download)

Unpack the content into the `/usr/bin` directory of the GIT MinGW installation. *For example, the standard install dir is*

    C:\Program Files\Git\usr\bin

It's also recommended to download/install the [tree](https://sourceforge.net/projects/gnuwin32/files/tree/1.5.2.2/tree-1.5.2.2-bin.zip/download) command, simply because it's a handy directory command.

## Proceed
Go ahead and install

* [SDKMAN](http://sdkman.io/install.html)

You can just type the following commands in a BASH window (requires `curl`).
However, I do recommend that you read the install and usage documentation
of SDKMAN.

    curl -s "https://get.sdkman.io" | bash
    source "$HOME/.sdkman/bin/sdkman-init.sh"
    sdk version

When SDKMAN is working, you can list available tools and their versions

    sdk list

# Install tools using SDKMAN
Install the following tools using SDKMAN

* A recent version of Java JDK (*AdoptOpenJDK (adpt)*, *HotSpot (hs)*)
* The latest version of Gradle

