package ribomation;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.RecursiveTask;

public class Task extends RecursiveTask<Counts> {
    private final Path dir;
    public Task(Path dir) { this.dir = dir; }

    @Override
    protected Counts compute() {
        var result = new Counts();
        try {
            return Files.list(dir)
                    .peek(p -> { if (Files.isRegularFile(p)) result.update(p); })
                    .filter(Files::isDirectory)
                    .peek(d -> result.folders++)
                    .map(d -> new Task(d).fork())
                    .map(f -> f.join())
                    .reduce(result, (a, b) -> a.merge(b))
                    ;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
