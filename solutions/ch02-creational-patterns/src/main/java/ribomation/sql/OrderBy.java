package ribomation.sql;

import java.util.ArrayList;
import java.util.List;

public class OrderBy implements SqlGenerator {
    private List<String> columnNames = new ArrayList<>();
    private boolean ASC = true;

    public OrderBy add(String column) {
        columnNames.add(column);
        return this;
    }

    public OrderBy desc() {
        ASC = false;
        return this;
    }

    @Override
    public String toSQL() {
        return String.format("ORDER BY %s %s",
                String.join(",", columnNames),
                ASC ? "ASC" : "DESC"
        );
    }

}
