package ribomation.sql;

public class Limit implements SqlGenerator {
    protected int maxNumValues = 0; // no limit

    public Limit(int maxNumValues) {
        this.maxNumValues = maxNumValues;
    }

    @Override
    public String toSQL() {
        return String.format("LIMIT %d", maxNumValues);
    }
}
