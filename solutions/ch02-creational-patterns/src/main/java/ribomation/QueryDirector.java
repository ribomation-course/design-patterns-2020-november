package ribomation;

import ribomation.sql.Expression;
import ribomation.sql.Limit;
import ribomation.sql.OrderBy;
import ribomation.sql.SqlBuilder;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class QueryDirector {
    private final SqlBuilder builder;
    public QueryDirector(SqlBuilder builder) {
        this.builder = builder;
    }

    public String parse(Map<String, Object> dsl) {
        var query = builder.mkQuery(tableName(dsl));

        columns(dsl).forEach(query::addColumn);
        expressions(dsl).forEach(query::addExpression);
        orderBy(dsl).ifPresent(query::orderBy);
        limit(dsl).ifPresent(query::limit);

        return query.toSQL();
    }

    protected String tableName(Map<String, Object> dsl) {
        return dsl.get("table").toString();
    }

    protected List<String> columns(Map<String, Object> dsl) {
        return (List<String>) dsl.get("columns");
    }

    protected List<Expression> expressions(Map<String, Object> dsl) {
        var exprLst = (List<Map<String, Object>>) dsl.get("where");
        if (exprLst == null) throw new IllegalArgumentException("missing WHERE clause");
        return exprLst.stream()
                .map(expr -> {
                    var col = expr.get("col").toString();
                    var op = Expression.Operator.mk(expr.get("op").toString());
                    var val = expr.get("val");
                    return builder.mkExpression(col, op, val);
                })
                .collect(Collectors.toList());
    }

    protected Optional<OrderBy> orderBy(Map<String, Object> dsl) {
        var sort = (List<Map<String, Object>>) dsl.get("sort");
        if (sort == null || sort.isEmpty()) return Optional.empty();

        var expr1 = (Map<String, Object>) sort.get(0);
        var col = expr1.get("col").toString();
        var order = builder.mkOrderBy().add(col);

        var asc = expr1.get("asc").toString();
        if (asc.equals("false")) order = order.desc();

        return Optional.of(order);
    }

    protected Optional<Limit> limit(Map<String, Object> dsl) {
        var limit = dsl.get("limit");
        if (limit == null) return Optional.empty();

        var max = ((Number) limit).intValue();
        return Optional.of(builder.mkLimit(max));
    }

}
