package ribomation.number;

public class NumberImpl {
    enum Type {INT, FLOAT}

    final Type type;
    int intValue;
    double floatValue;

    public NumberImpl(int value) {
        type = Type.INT;
        intValue = value;
        floatValue = value;
    }

    public NumberImpl(double value) {
        type = Type.FLOAT;
        floatValue = value;
        intValue = (int) floatValue;
    }
}
