package ribomation.domain;

import java.util.List;
import java.util.Optional;

public interface PersonDAO {
    Optional<Person> findById(String id);

    List<Person> findAllByName(String name);

    void save(Person person);

    void delete(Person person);
}
