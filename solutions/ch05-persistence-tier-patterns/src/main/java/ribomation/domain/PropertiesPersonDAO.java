package ribomation.domain;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class PropertiesPersonDAO implements PersonDAO {
    private static final String EXT = ".properties";
    private final Path dbDir;

    public PropertiesPersonDAO(Path dbDir) {
        this.dbDir = dbDir;
    }

    @Override
    public Optional<Person> findById(String id) {
        var file = Path.of(dbDir.toString(), id + EXT);
        if (Files.exists(file)) {
            return Optional.of(load(file));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public List<Person> findAllByName(String name) {
        try {
            return Files.list(dbDir)
                    .filter(path -> path.toString().endsWith(EXT))
                    .map(this::load)
                    .filter(person -> person.getName().toLowerCase().contains(name.toLowerCase()))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void save(Person person) {
        try (var out = Files.newOutputStream(pathOf(person))) {
            var props = person.toProps();
            props.remove("id");
            props.store(out, new Date().toString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(Person person) {
        var file = pathOf(person);
        try {
            Files.delete(file);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Person load(Path file) {
        try (var in = Files.newInputStream(file)) {
            var data = new Properties();
            data.setProperty("id", idOf(file));
            data.load(in);
            return Person.fromProps(data);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Path pathOf(Person person) {
        return Path.of(dbDir.toString(), person.getId() + EXT);
    }

    private String idOf(Path file) {
        var name = file.getFileName().toString();
        return name.substring(name.lastIndexOf(".") + 1);
    }

}
