package ribomation.domain;

import java.util.Properties;
import java.util.StringJoiner;

public class Person {
    private final String id;
    private String name;
    private int age;

    public Person(String id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public static Person fromProps(Properties data) {
        var id = data.getProperty("id", "???");
        var name = data.getProperty("name", "no-name");
        var age = Integer.parseInt(data.getProperty("age", "0"));
        return new Person(id, name, age);
    }

    public Properties toProps() {
        var data = new Properties();
        data.setProperty("id", id);
        data.setProperty("name", name);
        data.setProperty("age", Integer.toString(age));
        return data;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Person.class.getSimpleName() + "[", "]")
                .add("id='" + id + "'")
                .add("name='" + name + "'")
                .add("age=" + age)
                .toString();
    }
}
