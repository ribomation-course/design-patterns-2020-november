package ribomation;

import ribomation.domain.Person;
import ribomation.domain.PersonDAO;
import ribomation.domain.PropertiesPersonDAO;

import java.nio.file.Path;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.run(new PropertiesPersonDAO(Path.of("./db")));
    }

    void run(PersonDAO dao) {
        dao.save(new Person("nisse", "Nisse Hult", 42));

        System.out.println("-- findById(nisse) ---");
        dao.findById("nisse")
                .ifPresentOrElse(
                        person -> System.out.printf("found: %s%n", person),
                        () -> System.out.printf("'nisse' not found%n"));

        System.out.println("-- findById(kurt) ---");
        dao.findById("kurt")
                .ifPresentOrElse(
                        person -> System.out.printf("found: %s%n", person),
                        () -> System.out.printf("'kurt' not found%n"));

        System.out.println("-- save(tomten) ---");
        var p = new Person("tomten", "Tomte Nisse Prosit", 66);
        dao.save(p);
        System.out.println("-- findById(tomten) ---");
        dao.findById("tomten")
                .ifPresentOrElse(
                        person -> System.out.printf("found: %s%n", person),
                        () -> System.out.printf("'tomten' not found%n"));

        System.out.println("-- findAllByName(nisse) --");
        dao.findAllByName("nisse").forEach(System.out::println);

        System.out.println("-- delete(tomten) ---");
        dao.delete(p);
    }
}
