package ribomation.api;

import java.util.StringJoiner;

public class Item {
    final String productName;
    final int count;
    final double pricePerItem;

    public Item(String productName, int count, double pricePerItem) {
        if (productName == null) {
            throw new IllegalArgumentException("name must have a value");
        }
        if (count < 1) {
            throw new IllegalArgumentException("count must be positive: " + count);
        }
        if (pricePerItem < 1) {
            throw new IllegalArgumentException("price must be positive: " + count);
        }
        this.productName = productName;
        this.count = count;
        this.pricePerItem = pricePerItem;
    }

    @Override
    public String toString() {
        return String.format("%s: %d x %.2f kr", productName, count, pricePerItem);
    }

    public double total() {
        return count * pricePerItem;
    }
}
