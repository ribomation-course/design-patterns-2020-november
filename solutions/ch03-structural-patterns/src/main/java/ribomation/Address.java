package ribomation;

public class Address {
    final String street;
    final String postCode;
    final String city;

    public Address(String street, String postCode, String city) {
        this.street = street;
        this.postCode = postCode;
        this.city = city;
    }

    @Override
    public String toString() {
        return String.format("%s, %s %s", street, postCode, city);
    }

}
