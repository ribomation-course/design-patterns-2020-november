package ribomation.visitors;

import ribomation.laptop.*;
import ribomation.parts.CompositePart;
import ribomation.parts.Part;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.stream.JsonGenerator;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.Stack;

public class JsonVisitor implements Visitor {
    private final Stack<JsonObjectBuilder> scopes = new Stack<>();
    private JsonObject obj;

    @Override
    public void visit(CompositePart part) {
        if (part instanceof Laptop) {
            var pc = (Laptop) part;
            var node = Json.createObjectBuilder();
            node.add("name", pc.name);
            node.add("model", pc.model);
            scopes.push(node);
        } else if (part instanceof MotherBoard) {
            var mb = (MotherBoard) part;
            var node = Json.createObjectBuilder();
            node.add("name", mb.name);
            scopes.push(node);
        }
    }

    @Override
    public void visitEnd(CompositePart part) {
        if (part instanceof MotherBoard) {
            var node = scopes.pop();
            scopes.peek().add("mother-board", node);
        } else if (part instanceof Laptop) {
            var node = scopes.pop();
            obj = Json.createObjectBuilder()
                    .add("laptop", node)
                    .build();
        }
    }

    @Override
    public void visit(Part part) {
        if (part instanceof Screen) {
            var screen = (Screen) part;
            var json = Json.createObjectBuilder()
                    .add("name", screen.name)
                    .add("frequency", String.format("%d Hz", screen.frequency))
                    .add("resolution", String.format("%d x %d", screen.resolutionWith, screen.resolutionHeight));
            scopes.peek().add("screen", json);
        } else if (part instanceof Keyboard) {
            var keyboard = (Keyboard) part;
            var json = Json.createObjectBuilder()
                    .add("layout", keyboard.layout);
            scopes.peek().add("keyboard", json);
        } else if (part instanceof Touchpad) {
            var touchpad = (Touchpad) part;
            var json = Json.createObjectBuilder()
                    .add("dimension", String.format("%d x %d mm", touchpad.width, touchpad.height));
            scopes.peek().add("touchpad", json);
        } else if (part instanceof CPU) {
            var cpu = (CPU) part;
            var json = Json.createObjectBuilder()
                    .add("name", cpu.name)
                    .add("speed", String.format("%.1f GHz", cpu.GHz));
            scopes.peek().add("cpu", json);
        } else if (part instanceof Memory) {
            var memory = (Memory) part;
            var json = Json.createObjectBuilder()
                    .add("type", memory.type)
                    .add("speed", String.format("%d GB", memory.GB));
            scopes.peek().add("memory", json);
        } else if (part instanceof Fan) {
            var fan = (Fan) part;
            var json = Json.createObjectBuilder()
                    .add("name", fan.name);
            scopes.peek().add("fan", json);
        }
    }

    public void writeTo(Path file) {
        var factory = Json.createWriterFactory(Map.of(JsonGenerator.PRETTY_PRINTING, true));
        try (var out = factory.createWriter(Files.newBufferedWriter(file))) {
            out.writeObject(obj);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
