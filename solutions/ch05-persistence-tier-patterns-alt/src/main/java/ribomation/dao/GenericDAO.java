package ribomation.dao;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import static java.nio.file.Files.*;
import static java.util.stream.Collectors.toList;

public class GenericDAO<Type extends DomainClass> implements DAO<Type> {
    private static final String EXT = ".properties";
    private final Path dbDir;
    private final PropertiesMapper<Type> mapper;

    public GenericDAO(Path dbDir, Class clsType) {
        this.mapper = new PropertiesMapper<>(clsType);
        this.dbDir = dbDir;
        if (!exists(dbDir)) {
            try {
                createDirectories(dbDir);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public Optional<Type> findById(String id) {
        var file = pathOf(id);
        if (exists(file)) {
            return Optional.of(load(file));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public List<Type> findAll() {
        try {
            return Files.list(dbDir)
                    .filter(path -> path.toString().endsWith(EXT))
                    .map(this::load)
                    .collect(toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void save(Type obj) {
        try (var out = newOutputStream(pathOf(obj))) {
            var props = mapper.toProps(obj);
            props.store(out, new Date().toString());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void remove(String id) {
        try {
            delete(pathOf(id));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Type load(Path file) {
        try (var in = newInputStream(file)) {
            var data = new Properties();
            data.load(in);
            return mapper.fromProps(idOf(file), data);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private Path pathOf(String id) {
        return Path.of(dbDir.toString(), id + EXT);
    }

    private Path pathOf(DomainClass obj) {
        return pathOf(obj.getId());
    }

    private String idOf(Path file) {
        var name = file.getFileName().toString();
        return name.substring(0, name.lastIndexOf("."));
    }

}
