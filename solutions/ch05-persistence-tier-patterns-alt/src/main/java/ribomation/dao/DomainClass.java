package ribomation.dao;

public abstract class DomainClass {
    private final String id;

    public DomainClass(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

}
