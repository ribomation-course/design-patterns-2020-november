package ribomation.dao;

public class Dummy extends DomainClass {
    private String name;
    private int number;

    public Dummy(String id) {
        super(id);
    }

    public Dummy(String id, String name, int number) {
        super(id);
        this.name = name;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(String number) {
        setNumber(Integer.parseInt(number));
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
