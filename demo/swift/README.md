The SWIFT Threads Demo
====

How to run the demo
----

### *NIX

    ./gradlew unsafeSwift
    ./gradlew safeSwift

### Windows

    ./gradlew.bat unsafeSwift
    ./gradlew.bat safeSwift

N.B.
----
The build is based on Gradle and its wrapper.
There is no need to install Gradle, just use
the provided wrapper, as shown above.
