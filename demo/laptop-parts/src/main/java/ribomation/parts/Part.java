package ribomation.parts;

import ribomation.visitors.Visitor;

import java.util.Iterator;

public interface Part extends Iterable<Part> {
    default void accept(Visitor visitor) {
        visitor.visit(this);
    }

    default void add(Part part) { }

    @Override
    default Iterator<Part> iterator() {
        return new Iterator<>() {
            @Override
            public boolean hasNext() { return false; }
            @Override
            public Part next() { return null; }
        };
    }

    default String debug() {
        var txt = new StringBuilder();
        txt.append(getClass().getSimpleName()).append("{");
        for (var f : getClass().getDeclaredFields()) {
            try {
                txt.append(String.format("%s: %s", f.getName(), f.get(this).toString()));
            } catch (IllegalAccessException e) {
                txt.append("** ").append(e.toString()).append(" **");
            }
        }
        txt.append("}");
        return txt.toString();
    }
}



