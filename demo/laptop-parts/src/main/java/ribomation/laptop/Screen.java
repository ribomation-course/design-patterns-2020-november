package ribomation.laptop;

import ribomation.parts.Part;

public class Screen implements Part {
    public final String name;
    public final int resolutionWith;
    public final int resolutionHeight;
    public final int frequency;
    public Screen(String name, int resolutionWith, int resolutionHeight, int frequency) {
        this.name = name;
        this.resolutionWith = resolutionWith;
        this.resolutionHeight = resolutionHeight;
        this.frequency = frequency;
    }
}


