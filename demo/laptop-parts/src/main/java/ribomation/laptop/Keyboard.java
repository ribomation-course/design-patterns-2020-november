package ribomation.laptop;

import ribomation.parts.Part;

public class Keyboard implements Part {
    public final String layout;
    public Keyboard(String layout) {
        this.layout = layout;
    }
}
