package ribomation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import static java.lang.Double.parseDouble;
import static java.nio.file.Files.newInputStream;
import static java.time.LocalDate.parse;
import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.*;


public class ClimateData {
    public List<Map.Entry<Integer, Double>>
    computeYearlyAverages(Path path) throws IOException {
        var reader =
                new BufferedReader(
                        new InputStreamReader(
                                uncompressedByteCounter(
                                        new GZIPInputStream(
                                                compressedByteCounter(
                                                        newInputStream(path))))));
        try (reader) {
            return reader.lines().parallel()
                    .filter(line -> !line.startsWith("STN--"))
                    .map(line -> line.split("\\s+"))
                    .map(fields -> Pair.of(fields[2], fields[3]))
                    .map(pair -> {
                        var date = parse(pair.getKey(), DateTimeFormatter.BASIC_ISO_DATE);
                        var fahrenheit = parseDouble(pair.getValue());
                        var celsius = (5D / 9D) * (fahrenheit - 32D);
                        return Pair.of(date, celsius);
                    })
                    .collect(groupingBy(
                            p -> p.getKey().getYear(),
                            averagingDouble(Map.Entry::getValue)
                    ))
                    .entrySet().stream()
                    .sorted(comparingInt(Map.Entry::getKey))
                    .collect(toList());
        }
    }

    private InputStream compressedByteCounter(InputStream is) {
        return compressed = new CountingInputStream(is);
    }

    private InputStream uncompressedByteCounter(InputStream is) {
        return uncompressed = new CountingInputStream(is);
    }

    public long compressedByteCount() {
        return compressed.getByteCount();
    }

    public long uncompressedByteCount() {
        return uncompressed.getByteCount();
    }

    private CountingInputStream compressed;
    private CountingInputStream uncompressed;
}
