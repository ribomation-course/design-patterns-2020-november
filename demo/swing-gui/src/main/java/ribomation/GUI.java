package ribomation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

import static java.awt.BorderLayout.*;

public class GUI extends JFrame {
    private final String path;
    public GUI(String path) throws HeadlessException {
        super("Simple Swing GUI");
        this.path = path;
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
    }

    public void setup() {
        setJMenuBar(mkMenuBar());
        add(mkHeader(), NORTH);
        add(mkContent(path), CENTER);
        add(mkFooter(), SOUTH);
    }

    private JMenuBar mkMenuBar() {
        var fileMenu = new JMenu("File");
        fileMenu.add(new AbstractAction("Quit") {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        var mb = new JMenuBar();
        mb.add(fileMenu);
        return mb;
    }

    private JPanel mkHeader() { return new Header(); }
    private JPanel mkContent(String path) {
        return new PersonsTable(path);
    }
    private JPanel mkFooter() { return new Footer(); }
}


