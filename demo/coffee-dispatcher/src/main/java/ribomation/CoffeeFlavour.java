package ribomation;

import java.util.Optional;
import java.util.WeakHashMap;

public class CoffeeFlavour {
    enum Cream {dash, tablespoon}
    enum Sugar {pinch, teaspoon}

    public final String name;
    public final Optional<Cream> cream;
    public final Optional<Sugar> sugar;

    private CoffeeFlavour(String name, Cream cream, Sugar sugar) {
        this.name = name;
        this.cream = Optional.ofNullable(cream);
        this.sugar = Optional.ofNullable(sugar);
    }

    @Override
    public String toString() {
        return String.format("Cup of %s with %s cream and %s sugar",
                name,
                cream.isEmpty() ? "no" : "a " + cream.get() + " of",
                sugar.isEmpty() ? "no" : "a " + sugar.get() + " of"
        );
    }

    private static final WeakHashMap<Integer, CoffeeFlavour> cache = new WeakHashMap<>();

    public static CoffeeFlavour get(String name, Cream cream, Sugar sugar) {
        var key = String.format("%s:%s:%s", name, cream, sugar).hashCode();
        return cache.computeIfAbsent(key, __ -> new CoffeeFlavour(name, cream, sugar));
    }

    public static int cacheSize() { return cache.size(); }
}


