package ribomation.sql.mysql;

import ribomation.sql.*;

public class MySqlBuilder implements SqlBuilder {
    @Override
    public Query mkQuery(String table) {
        return new Query(table);
    }

    @Override
    public Expression mkExpression(String columnName, Expression.Operator operator, Object value) {
        return new Expression(columnName, operator, value);
    }

    @Override
    public OrderBy mkOrderBy() {
        return new OrderBy();
    }

    @Override
    public Limit mkLimit(int max) {
        return new Limit(max);
    }
}


