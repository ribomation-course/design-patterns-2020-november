package ribomation.sql;

public class Expression implements SqlGenerator {

    public enum Operator {
        equals("="), lessThan("<"), greaterThat(">");

        private final String symbol;
        Operator(String symbol) {
            this.symbol = symbol;
        }

        @Override
        public String toString() {
            return symbol;
        }

        public static Operator mk(String op) {
            switch (op) {
                case "EQ":
                    return Operator.equals;
                case "LT":
                    return Operator.lessThan;
                case "GT":
                    return Operator.greaterThat;
            }
            throw new IllegalArgumentException("no such op: " + op);
        }
    }

    private final String columnName;
    private final Operator operator;
    private final Object value;

    public Expression(String columnName, Operator operator, Object value) {
        this.columnName = columnName;
        this.operator = operator;
        this.value = value;
    }

    @Override
    public String toSQL() {
        return String.format("%s %s %s", columnName, operator, quoted(value));
    }

    protected String quoted(Object val) {
        if (val instanceof String) {
            return String.format("'%s'", val.toString());
        } else if (val instanceof Integer) {
            return String.format("%d", ((Number) val).intValue());
        } else if (val instanceof Float) {
            return String.format("%.3f", ((Number) val).floatValue());
        } else if (val instanceof Boolean) {
            return String.format("%b", (Boolean)val);
        }
        throw new IllegalArgumentException("unrecognized value type: " + val);
    }
}
