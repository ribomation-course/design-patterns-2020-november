package ribomation.sql;

public interface SqlBuilder {
    Query mkQuery(String table);

    Expression mkExpression(String columnName, Expression.Operator operator, Object value);

    OrderBy mkOrderBy();

    Limit mkLimit(int max);
}
