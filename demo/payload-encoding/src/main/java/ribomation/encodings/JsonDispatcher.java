package ribomation.encodings;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import ribomation.Car;
import ribomation.Dispatcher;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class JsonDispatcher extends Dispatcher {
    private final Gson gson;
    private final Type listType = new TypeToken<ArrayList<Car>>() {}.getType();

    public JsonDispatcher() {
        gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
    }

    @Override
    protected List<Car> load(Path file) {
        try (var in = Files.newBufferedReader(file)) {
            return gson.fromJson(in, listType);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void store(Path file, List<Car> cars) {
        try (var out = Files.newBufferedWriter(file)) {
            gson.toJson(cars, out);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}


