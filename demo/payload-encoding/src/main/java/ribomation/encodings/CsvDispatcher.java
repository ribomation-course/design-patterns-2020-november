package ribomation.encodings;

import ribomation.Car;
import ribomation.Dispatcher;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import static java.nio.file.Files.newBufferedReader;
import static java.nio.file.Files.newBufferedWriter;
import static java.util.stream.Collectors.toList;

public class CsvDispatcher extends Dispatcher {
    public CsvDispatcher() { }

    @Override
    protected List<Car> load(Path file) {
        try (var in = newBufferedReader(file)) {
            return in.lines()
                    .skip(1) //header line
                    .map(csv -> csv.split(","))
                    .map(f -> new Car(f[0], f[1], Integer.valueOf(f[2]), f[3]))
                    .collect(toList());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void store(Path file, List<Car> cars) {
        try (var out = newBufferedWriter(file)) {
            cars.forEach(c -> {
                var csv = String.format("%s;%s;%d;%s%n", c.maker, c.model, c.year, c.country);
                try {
                    out.write(csv);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}



