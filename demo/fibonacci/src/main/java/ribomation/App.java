package ribomation;

public class App {
    public static void main(String[] args) {
        var app = new App();
        app.parseArgs(args);
        app.run();
    }

    int argument = 10;
    boolean iterative = false;
    void parseArgs(String[] args) {
        for (int k = 0; k < args.length; ++k) {
            if (args[k].equals("--arg")) {
                argument = Integer.parseInt(args[++k]);
            } else if (args[k].equals("--iter")) {
                iterative = true;
            }
        }
    }
    void run() {
        if (argument < 0) {
            throw new IllegalArgumentException("argument must be positive: " + argument);
        }
        compute(mkFibonacci(), argument);
    }

    void compute(Fibonacci f, int n) {
        var startTime = System.nanoTime();
        var result = f.compute(n);
        var endTime = System.nanoTime();
        System.out.printf("fib(%d) = %s%n", n, result);
        System.out.printf("elapsed %.3f ms (%s)%n",
                (endTime - startTime) * 1E-6,
                iterative ? "iterative" : "recursive"
                );
    }

    Fibonacci mkFibonacci() {
        if (iterative) return new IterativeFibonacci();
        return new RecursiveFibonacci();
    }
}



