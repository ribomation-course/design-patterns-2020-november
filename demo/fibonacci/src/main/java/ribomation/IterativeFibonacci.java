package ribomation;

import java.math.BigInteger;

public class IterativeFibonacci implements Fibonacci {
    @Override
    public BigInteger compute(int n) {
        var f2 = BigInteger.ZERO;
        var f1 = BigInteger.ONE;
        while (n-- > 0) {
            var f = f2.add(f1);
            f2 = f1;
            f1 = f;
        }
        return f2;
    }
}
