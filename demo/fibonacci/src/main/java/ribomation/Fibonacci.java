package ribomation;

import java.math.BigInteger;

public interface Fibonacci {
    BigInteger compute(int n);
}


