package ribomation.gui;

import ribomation.domain.PersonRepo;

import javax.swing.table.AbstractTableModel;

public class PersonsModel extends AbstractTableModel {

    @Override
    public int getRowCount() {
        return PersonRepo.instance.getPersons().size();
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int row, int column) {
        var p = PersonRepo.instance.getPersons().get(row);
        switch (column) {
            case 0:
                return p.fname;
            case 1:
                return p.lname;
            case 2:
                return p.email;
            case 3:
                return p.city;
        }
        throw new ArrayIndexOutOfBoundsException(column);
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "First Name";
            case 1:
                return "Last Name";
            case 2:
                return "Email";
            case 3:
                return "City";
        }
        throw new ArrayIndexOutOfBoundsException(column);
    }
}



