package ribomation.gui;

import javax.swing.*;
import java.awt.*;

import static java.awt.BorderLayout.CENTER;

public class PersonsTable extends JPanel {
    public PersonsTable() {
        super(new BorderLayout());
        var tbl = new JTable(new PersonsModel());
        add(new JScrollPane(tbl), CENTER);
    }
}



