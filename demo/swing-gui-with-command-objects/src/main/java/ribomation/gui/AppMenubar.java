package ribomation.gui;

import ribomation.commands.Command;

import javax.swing.*;

public class AppMenubar extends JMenuBar {
    public AppMenubar() {
        var fileMenu = new JMenu("File");
        fileMenu.add(Command.get("Quit").newMenuItem());
        add(fileMenu);

        var showMenu = new JMenu("Show");
        showMenu.add(Command.get("SillyMessage").newMenuItem());
        add(showMenu);
    }
}


