package ribomation.commands;

import ribomation.gui.GUI;

import javax.swing.*;

import static javax.swing.JOptionPane.INFORMATION_MESSAGE;

public class SillyMessage extends Command {
    public SillyMessage() {
        setLabel("Silly Message");
        setMnemonic('s');
        setTooltip("Shows a silly message");
        setIcon("about");
    }

    @Override
    protected void doExecute() {
        JOptionPane.showMessageDialog(GUI.instance,
                "Just a silly message",
                "Message",
                INFORMATION_MESSAGE);
    }
}


