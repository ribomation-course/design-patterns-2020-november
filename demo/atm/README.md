The ATM Threads Demo
====

How to run the demo
----

### *NIX

    ./gradlew unsafeBank
    ./gradlew safeBank

### Windows

    gradlew.bat unsafeBank
    gradlew.bat safeBank

N.B.
----
The build is based on Gradle and its wrapper.
There is no need to install Gradle, just use
the provided wrapper, as shown above.
