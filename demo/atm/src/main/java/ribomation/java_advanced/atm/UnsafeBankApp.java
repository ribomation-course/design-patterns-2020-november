package ribomation.java_advanced.atm;

public class UnsafeBankApp extends App {
    public static void main(String[] args) {
        new UnsafeBankApp().run(args);
    }

    @Override
    protected Account createAccount() {
        return new Account();
    }
}
