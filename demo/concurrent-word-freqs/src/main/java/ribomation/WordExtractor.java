package ribomation;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Path;
import java.util.Iterator;

public class WordExtractor implements Iterable<String> {
    private final Path path;
    private final int offset;
    private final int chunkSize;

    public WordExtractor(Path path, int offset, int chunkSize) {
        this.path = path;
        this.offset = offset;
        this.chunkSize = chunkSize;
    }

    @Override
    public Iterator<String> iterator() {
        try {
            return new WordIterator();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private class WordIterator implements Iterator<String> {
        private final byte[] chunk;
        private final int max;
        private int start = 0;
        private String word;

        public WordIterator() throws IOException {
            try (var file = new RandomAccessFile(path.toFile(), "r")) {
                file.seek(offset);
                chunk = new byte[chunkSize];
                max = file.read(chunk);
            }
            word = extract();
        }

        @Override
        public boolean hasNext() {
            return start < max;
        }

        @Override
        public String next() {
            var current = word;
            word = extract();
            return current;
        }

        private String extract() {
            while (start < max && !Character.isLetter((char) chunk[start])) ++start;

            var end = start;
            while (end < max && Character.isLetter((char) chunk[end])) ++end;

            var token = new String(chunk, start, end - start);
            start = end + 1;
            return token;
        }
    }
}
