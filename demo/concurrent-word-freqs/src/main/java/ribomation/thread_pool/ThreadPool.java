package ribomation.thread_pool;

import ribomation.AppVariant;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ThreadPool implements AppVariant {
    public static void main(String[] args) throws Exception {
        var filename = "./data/musketeers.txt";
        if (args.length > 0) filename = "./data/english.50MB.txt";

        new ThreadPool().run(filename, 25);
    }

    public void run(String filename, int maxWords) throws Exception {
        var numCPUs = Runtime.getRuntime().availableProcessors();
        var numTasks = 25 * numCPUs;
        var filePath = Path.of(filename);
        var chunkSize = Files.size(filePath) / numTasks;
        var tasks = new ArrayList<Task>();
        for (var k = 0; k < numTasks; ++k) {
            tasks.add(new Task(filePath, k * chunkSize, chunkSize));
        }

        var pool = Executors.newFixedThreadPool(numCPUs);
        var futures = pool.invokeAll(tasks);
        var freqs = new HashMap<String, Integer>();
        futures.forEach(f -> {
            try {
                var partial = f.get();
                partial.forEach((word, count) -> freqs.put(word, count + freqs.getOrDefault(word, 0)));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
        freqs.entrySet().stream()
                .sorted((lhs, rhs) -> rhs.getValue() - lhs.getValue())
                .limit(maxWords)
                .forEach(e -> System.out.printf("%s: %s%n", e.getKey(), e.getValue()));

        pool.shutdown();
        pool.awaitTermination(1, TimeUnit.SECONDS);
    }

}
