package ribomation.fork_join;

import ribomation.WordExtractor;

import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.RecursiveTask;

public class Task extends RecursiveTask<Map<String, Integer>> {
    private final Path path;
    private final int offset;
    private final int chunkSize;
    public static int MIN_CHUNK_SIZE = 10240;
    public static int MIN_WORD_SIZE = 5;

    public Task(Path path, int offset, int chunkSize) {
        this.path = path;
        this.offset = offset;
        this.chunkSize = chunkSize;
    }

    @Override
    protected Map<String, Integer> compute() {
        if (chunkSize <= MIN_CHUNK_SIZE) return computeUnitWork();

        var size = chunkSize / 2;
        var left = new Task(path, offset, size).fork();
        var right = new Task(path, offset + size + 1, size).fork();

        return merge(left.join(), right.join());
    }

    private Map<String, Integer> computeUnitWork() {
        var freqs = new HashMap<String, Integer>();
        for (var word : new WordExtractor(path, offset, chunkSize)) {
            word = word.toLowerCase();
            if (word.length() >= MIN_WORD_SIZE) {
                freqs.put(word, 1 + freqs.getOrDefault(word, 0));
            }
        }
        return freqs;
    }

    private Map<String, Integer> merge(Map<String, Integer> left, Map<String, Integer> right) {
        left.forEach((word, count) -> right.put(word, count + right.getOrDefault(word, 0)));
        return right;
    }
}



