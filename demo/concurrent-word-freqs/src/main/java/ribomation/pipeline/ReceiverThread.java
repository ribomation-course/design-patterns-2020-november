package ribomation.pipeline;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public abstract class ReceiverThread<T> extends Thread {
    private final BlockingQueue<T> queue;
    private boolean done = false;
    public ReceiverThread() {
        queue = new ArrayBlockingQueue<>(512);
    }

    public void send(T msg) {
        if (msg == null || done) { done = true; return; }
        try {
            queue.put(msg);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    protected T recv() {
        if (done && queue.isEmpty()) return null;
        try {
            return queue.take();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}


