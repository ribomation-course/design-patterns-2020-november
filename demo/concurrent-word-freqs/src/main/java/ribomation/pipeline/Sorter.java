package ribomation.pipeline;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;

public class Sorter extends ReceiverThread<Map<String, Integer>> {
    private final ReceiverThread<List<Map.Entry<String, Integer>>> next;

    public Sorter(ReceiverThread<List<Map.Entry<String, Integer>>> next) {
        this.next = next;
        start();
    }

    @Override
    public void run() {
        var sorted = recv().entrySet().stream()
                .sorted((lhs, rhs) -> rhs.getValue() - lhs.getValue())
                .collect(toList());
        next.send(sorted);
    }
}
